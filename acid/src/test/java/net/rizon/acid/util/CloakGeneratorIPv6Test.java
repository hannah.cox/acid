package net.rizon.acid.util;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Tests the cloak generator functionality for IPv6 addresses.
 * <p>
 * @author Orillion {@literal <orillion@rizon.net>}
 */
@RunWith(Parameterized.class)
public class CloakGeneratorIPv6Test
{
	private static final String KEY_ONE = "cloakKey1";
	private static final String KEY_TWO = "cloakKey2";
	private static final String KEY_THREE = "cloakKey3";
	private static final String NETWORK = "Rizon-";

	private static final String KEYS[] =
	{
		KEY_ONE, KEY_TWO, KEY_THREE
	};

	private final String ipv6;
	private final String expected;

	private CloakGenerator cloakGenerator;

	/**
	 * Constructor, used to run the parameterized tests.
	 * <p>
	 * @param ipv6     ipv6 address to test.
	 * @param expected expected cloaked ipv6 address.
	 */
	public CloakGeneratorIPv6Test(String ipv6, String expected)
	{
		this.ipv6 = ipv6;
		this.expected = expected;
	}

	/**
	 * Constructs a new {@link CloakGenerator} for each test.
	 */
	@Before
	public void setUp()
	{
		this.cloakGenerator = new CloakGenerator(KEYS, NETWORK);
	}

	/**
	 * Clears the {@link CloakGenerator} after each test.
	 */
	@After
	public void tearDown()
	{
		this.cloakGenerator = null;
	}

	/**
	 * List of ipv6 addresses to test. Contains input and expected results.
	 * <p>
	 * @return A collection of inputs and their expected result.
	 */
	@Parameterized.Parameters
	public static Collection ips6()
	{
		return Arrays.asList(new Object[][]
		{
			{
				// IPv4 mapping
				"::ffff:44.55.66.77", "4D24B2D0:7AAF89D7:6983D238:IP"
			},
			{
				"::beef", "41D07F22:7AAF89D7:6983D238:IP"
			},
			{
				"beef::beef", "68A02068:C629A32D:ECCA85A7:IP"
			},
			{
				"0:0:0:0:0:0:0:0", "A546741B:7AAF89D7:6983D238:IP"
			},
			{
				"ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", "923A446F:6E326E59:72EEFF3:IP"
			},
			{
				"abcd:1234:12:34:45AB:CCDD:10::", "9D0F31C4:A8DA7EAA:E8AD9905:IP"
			},
			{
				"abcd:1234:12:34:45AB:CCDD:10:0", "E9F7A7C1:A8DA7EAA:E8AD9905:IP"
			},
			{
				"abcd:1234:12:34:45AB:CCDD:10:0000", "B54250D4:A8DA7EAA:E8AD9905:IP"
			}
		});
	}

	/**
	 * Test function, reused for each parameter.
	 */
	@Test
	public void testIPv6Cloak()
	{
		String result = this.cloakGenerator.cloak(this.ipv6);
		Assert.assertEquals(this.expected, result);
	}
}

package net.rizon.acid.core;

public class Membership
{
	public User user;
	public Channel channel;
	public String modes = "";

	public Membership(User user, Channel channel, String modes)
	{
		this.user = user;
		this.channel = channel;
		if (modes == null)
			this.modes = "";
		else
			this.modes = modes;
	}

	public void clear()
	{
		modes = "";
	}

	public void addMode(String mode)
	{
		if (mode == null || mode.equals(""))
			return;
		String c;
		for (int i = 0; i < mode.length(); i++)
		{
			c = mode.substring(i, i + 1);
			if (!c.matches("(q|a|o|h|v)") || modes.contains(c))
				;
			else
				modes += c;
		}
	}

	public void remMode(String mode)
	{
		if (mode == null || mode.equals(""))
			return;
		String c;
		for (int i = 0; i < mode.length(); i++)
		{
			c = mode.substring(i, i + 1);
			if (!c.matches("(q|a|o|h|v)") || !modes.contains(c))
				;
			else
				modes = modes.replaceAll(c, "");
		}
	}

	public boolean isReg()
	{
		if (modes == null || modes.equals(""))
			return true;
		return false;
	}

	public boolean isOp()
	{
		if (modes != null && !modes.equals("") && (modes.contains("h") || modes.contains("o") ||
				modes.contains("a") || modes.contains("q")))
			return true;
		return false;
	}

	public boolean hasVoice()
	{
		if (modes != null && !modes.equals("") && modes.contains("v"))
			return true;
		return false;
	}

	public boolean hasHalfop()
	{
		if (modes != null && !modes.equals("") && modes.contains("h"))
			return true;
		return false;
	}

	public boolean hasOp()
	{
		if (modes != null && !modes.equals("") && modes.contains("o"))
			return true;
		return false;
	}

	public boolean hasAdmin()
	{
		if (modes != null && !modes.equals("") && modes.contains("a"))
			return true;
		return false;
	}

	public boolean hasOwner()
	{
		if (modes != null && !modes.equals("") && modes.contains("q"))
			return true;
		return false;
	}

	public String getModes()
	{
		if (modes == null || modes.equals(""))
			return "";
		String m = "";
		if (hasOwner())
			m += "~";
		if (hasAdmin())
			m += "&";
		if (hasOp())
			m += "@";
		if (hasHalfop())
			m += "%";
		if (hasVoice())
			m += "+";
		return m;
	}
}

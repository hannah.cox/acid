package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;
import net.rizon.acid.util.Blowfish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Privmsg extends Message
{
	private static final Logger log = LoggerFactory.getLogger(Privmsg.class);

	public Privmsg()
	{
		super("PRIVMSG");
	}

	// :99hAAAAAB PRIVMSG moo :hi there

	@Override
	public void onUser(User sender, String[] params)
	{
		String target = params[0], msg = params[1];

		if (msg.startsWith("\1") && msg.endsWith("\1"))
			Acidictive.onCtcp(sender.getNick(), target, msg);
		else
		{
			String crypto_pass = Acidictive.conf.getCryptoPass(target);
			if (crypto_pass != null)
			{
				try
				{
					Blowfish bf = new Blowfish(crypto_pass);
					String dec = bf.Decrypt(msg);
					if (dec == null)
						return; // Don't process non-encrypted messages sent to targets expecting encrypted messages

					msg = dec;
					log.debug("Blowfish decrypted: {}", msg);
				}
				catch (Exception e)
				{
					log.warn("Unable to blowfish decrypt: " + msg, e);
					return; // Don't process non-encrypted messages sent to targets expecting encrypted messages
				}
			}

			Acidictive.onPrivmsg(sender.getNick(), params[0], msg);
		}
	}
}
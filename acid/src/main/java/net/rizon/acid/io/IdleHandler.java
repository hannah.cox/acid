package net.rizon.acid.io;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Protocol;
import org.slf4j.LoggerFactory;

public class IdleHandler extends SimpleChannelInboundHandler
{
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(SimpleChannelInboundHandler.class);

	private boolean idle;
	
	@Override
	public void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception
	{
		idle = false;

		ctx.fireChannelRead(msg);
	}
	
	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception
	{
		if (evt instanceof IdleStateEvent)
		{
			IdleStateEvent e = (IdleStateEvent) evt;

			if (e.state() == IdleState.READER_IDLE)
			{
				if (!idle)
				{
					Protocol.ping(Acidictive.uplink);
					idle = true;
				}
				else
				{
					log.warn("No read from uplink in 120 seconds, closing connection");

					ctx.close();
				}
			}

			return;
		}

		ctx.fireUserEventTriggered(evt);
	}
}

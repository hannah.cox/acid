package net.rizon.acid.events;

import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;

public class EventJoin
{
	private Channel channel;
	private User[] users;

	public Channel getChannel()
	{
		return channel;
	}

	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}

	public User[] getUsers()
	{
		return users;
	}

	public void setUsers(User[] users)
	{
		this.users = users;
	}
}

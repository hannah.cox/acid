package net.rizon.acid.events;

import net.rizon.acid.core.Server;

public class EventEOB
{
	private Server server;

	public Server getServer()
	{
		return server;
	}

	public void setServer(Server server)
	{
		this.server = server;
	}
}

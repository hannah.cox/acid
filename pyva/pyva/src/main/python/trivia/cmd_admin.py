import pseudoclient.sys_base

import os
import yaml
from datetime import datetime
from utils import *

#---------------------------------------------------------------------#
# import admin cmds and re-export commands we don't need to override

from pseudoclient import cmd_admin
from pseudoclient.cmd_admin import \
	admin_unregistered, \
	admin_chan, \
	admin_log, \
	admin_msg, \
	admin_opt, \
	admin_db

#---------------------------------------------------------------------#

PATH_TO_TRIVIA = "pyva/pyva/src/main/python/trivia/"
TRIVIA_YML_DB = PATH_TO_TRIVIA + "trivia.yml"

def admin_sys(self, source, target, pieces):
	if len(pieces) < 2:
		return False

	starget = pieces[0]
	operation = pieces[1]
	names = []
	subsystems = []

	if 'o' in starget:
		names.append('options')
		subsystems.append(self.options)

	if 'c' in starget:
		names.append('channels')
		subsystems.append(self.channels)

	if 'a' in starget:
		names.append('auth')
		subsystems.append(self.auth)

	if len(names) == 0:
		return False

	if operation in ['u', 'update']:
		for subsystem in subsystems:
			subsystem.force()

		self.msg(target, 'Forced update for @b%s@b.' % '@b, @b'.join(names))
	elif operation in ['r', 'reload']:
		for subsystem in subsystems:
			subsystem.reload()

		self.msg(target, 'Forced reload for @b%s@b.' % '@b, @b'.join(names))
	else:
		return False

	return True

def admin_stats(self, source, target, pieces):
	self.msg(target, 'Registered channels: @b%d@b.' % len(self.channels.list_all()))
	return True

def admin_qsize(self, source, target, pieces):
	self.msg(target, 'Queue size: %d' % len(self.limit_monitor))
	return True

def admin_import_yml(self, source, target, pieces):
	if not os.path.exists(TRIVIA_YML_DB):
		self.msg(target, "Error: file '{}' does not exist.".format(TRIVIA_YML_DB))
		return
	self.msg(target, "Importing Trivia DB from YML to MySQL: starting.")
	self.dbp.execute("DROP TABLE IF EXISTS trivia_questions")
	self.dbp.execute("DROP TABLE IF EXISTS trivia_themes")

	query = """
	CREATE TABLE `trivia_questions` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`question` varchar(512) NOT NULL,
	`answer` varchar(512) NOT NULL,
	`theme_id` int(11) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `theme_id` (`theme_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"""
	self.dbp.execute(query)

	query = """
	CREATE TABLE `trivia_themes` (
	`theme_id` int(11) NOT NULL AUTO_INCREMENT,
	`theme_name` varchar(32) NOT NULL,
	PRIMARY KEY (`theme_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"""
	self.dbp.execute(query)

	query = "ALTER TABLE `trivia_questions` ADD CONSTRAINT `fk_theme` FOREIGN KEY (`theme_id`) REFERENCES `trivia_themes` (`theme_id`)"
	self.dbp.execute(query)

	with open(TRIVIA_YML_DB, "r") as trivia_fp:
		yml_db = yaml.load(trivia_fp)
		for theme in yml_db["trivia_themes"]:
			query = "INSERT INTO `trivia_themes` (theme_name) VALUES ('{}')"
			self.dbp.execute(query.format(theme['name']))

			tid = self.dbp.lastrowid

			for question in theme['questions']:
				query = "INSERT INTO `trivia_questions` (question, answer, theme_id) VALUES (%s, %s, %s)"
				self.dbp.execute(query, [question['question'], question['answer'], tid])
	self.msg(target, "Importing: complete.")

def admin_export_yml(self, source, target, pieces):
	self.msg(target, "Exporting Trivia DB from MySQL to YML: starting.")
	trivia_export_filename = TRIVIA_YML_DB
	with open(trivia_export_filename, "w") as trivia_fp:
		yml_db = {"trivia_themes":[]}
		query = "SELECT `theme_id`, `theme_name` from `trivia_themes` ORDER BY `theme_id` ASC"
		self.dbp.execute(query)
		themes = self.dbp.fetchall()
		for theme in themes:
			t = { 'name' : theme[1],
					'questions' : []
					}
			query = "SELECT question,answer FROM `trivia_questions` WHERE theme_id = " + str(theme[0])
			self.dbp.execute(query)
			questions = self.dbp.fetchall()
			for question in questions:
				q = { 'question' : question[0], 'answer' : question[1] }
				t["questions"].append(q)

			yml_db["trivia_themes"].append(t)
		yml_dump = yaml.safe_dump(yml_db, default_flow_style=False, allow_unicode = True)
		trivia_fp.write(yml_dump)
	self.msg(target, "Exporting: complete.")

def get_commands():
	return {
		'chan'       : (admin_chan,            '<ban|unban|info|add|remove|list|blist> <channel> [reason]'),
		'unreg'      : (admin_unregistered,    '<check|list|part> - remove unregistered channels'),
		'stats'      : (admin_stats,           'counts registered channels'),
		'db'         : (admin_db,              '[on|off] - enables/disables auto commits to db'),
		'opt'        : (admin_opt,             '[get|set|clear] [option] [value] - manipulates options (list all if no arguments)'),
		'sys'        : (admin_sys,             '<subsystem> <operation> [value] - (subsystems: options (o), channels (c), auth (a)) (operations: update (u), reload (r))'),
		'log'        : (admin_log,             '[level] - gets or sets the log level (0-7).'),
		'msg'        : (admin_msg,             '<message> - sends a message to all channels'),
		'qsize'      : (admin_qsize,           '[debug command] shows channel update queue size'),
		'importdb'   : (admin_import_yml,      'populate trivia (questions/themes) mysql tables from trivia.yml'),
		'exportdb'   : (admin_export_yml,      'export the trivia (questions/themes) mysql tables to "trivia.yml"'),
	}

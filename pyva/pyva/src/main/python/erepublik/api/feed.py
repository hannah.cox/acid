import json
import socket
import urllib2
from BaseHTTPServer import BaseHTTPRequestHandler
from decimal import Decimal
from StringIO import StringIO
from urlparse import urlparse
from xml.dom.minidom import Element, Document
from lxml import etree

class InputError(Exception):
	def __init__(self, msg):
		self.msg = msg

	def __str__(self):
		return str(self.msg)

class FeedError(Exception):
	def __init__(self, e):
		if hasattr(e, 'code'):
			c = e.code

			if c == 404:
				self.msg = 'not found.'
			elif c == 406:
				self.msg = 'this eRepublik API feed is unavailable.'
			elif c == 500:
				self.msg = 'eRepublik server has encountered an unexpected error.'
			elif c == 502:
				self.msg = 'invalid response from eRepublik server. Try again later.'
			elif c == 503:
				self.msg = 'eRepublik API feed is temporarily unavailable. Try again later.'
			else:
				self.msg = 'something went wrong while connecting to eRepublik API feed (%s)' % BaseHTTPRequestHandler.responses[e.code][0]

			self.code = c
			self.url = e.url
		elif hasattr(e, 'reason'):
			r = str(e.reason)

			if r == 'timed out':
				self.msg = 'connection to eRepublik API feed timed out. Try again later.'
			else:
				self.msg = r

			self.code = None
			self.url = None
		else:
			e = unicode(e)
			
			if e == 'an error occured':
				self.msg = 'this command cannot work because eRepublik admins disabled the API. If you want to complain, send a ticket: http://www.erepublik.com/en/tickets'
			else:
				self.msg = e
			
			self.code = None
			self.url = None
	
	def __str__(self):
		return self.msg

class HtmlFeed:
	def __init__(self, value, fake_ua=False):
		if value == None:
			raise InputError('Invalid feed input.')

		if isinstance(value, str) or isinstance(value, unicode):
			try:
				opener = urllib2.build_opener()
				if fake_ua:
					opener.addheaders = [('User-Agent', 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1')]
				else:
					opener.addheaders = [('User-Agent', 'Rizon eRepublik bot - www.rizon.net')]
				feed = opener.open(value.replace(' ', '%20'), timeout=20)
				self._html = feed.read()
				feed.close()
			except urllib2.URLError, e:
				raise FeedError(e)
		else:
			raise InputError('Invalid feed input type.')

	def html(self):
		return self._html

def get_json(value):
	if value == None:
		raise InputError('Invalid feed input.')
	
	if isinstance(value, basestring):
		feed = HtmlFeed(value)
		return json.load(StringIO(feed.html()))
	else:
		raise InputError('Invalid feed input type.')

class XmlFeed:
	def __init__(self, value, namespaces = None):
		if value == None:
			raise InputError('Invalid feed input.')

		self.namespaces = {} if namespaces == None else namespaces

		if isinstance(value, basestring):
			feed = HtmlFeed(value)
			self._element = etree.parse(StringIO(feed.html()))
		elif isinstance(value, (Element, Document)):
			self._element = etree.parse(StringIO(value.toxml()))
		elif isinstance(value, (etree._Element, etree._ElementTree)):
			self._element = value
		else:
			raise InputError('Invalid feed input type [{}].'.format(type(value)))

		error = self._element.xpath('/error/message')

		if len(error):
			raise FeedError(error[0])

	def children(self):
		return [x for x in self._element]

	def elements(self, query):
		return [XmlFeed(x, self.namespaces) for x in self._element.xpath(query, namespaces=self.namespaces)]

	def text(self, query, default=None):
		results = self._element.xpath(query, namespaces=self.namespaces)

		if results and results[0].text is not None:
			value = results[0].text.strip()
		else:
			value = default

		if isinstance(value, unicode):
			try:
				value = value.encode('latin-1').decode('utf-8')
			except:
				pass
		
		return value

	def int(self, query, default = None):
		result = self.text(query, None)

		if result == None:
			return default

		try:
			return int(result)
		except:
			return default

	def decimal(self, query, default = None):
		result = self.text(query, None)

		if result == None:
			return default

		try:
			return Decimal(result)
		except:
			return default

	def bool(self, query, default = None):
		result = self.text(query, None)

		if result == None:
			return default

		if 'true' in result.lower() or result == '1':
			return True
		elif 'false' in result.lower() or result == '0':
			return False
		else:
			try:
				return int(result) > 0
			except:
				return default

	def attribute(self, query, attr, default = None, checkEveryOccurrence = False):
		elements = self._element.xpath(query)

		if len(elements) > 0 and not checkEveryOccurrence:
			return elements[0].attrib.get(attr, default)

		else:
			for e in elements:
				if attr in e.attrib.keys():
					return e.attrib.get(attr, default)
		return default
